@extends('layouts.master')
@section('content')
    <div class="container">
<div class="ml-4 mr-4">
<h2>Tambah Kategori</h2>
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama Kategori</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$kategori->nama}}" placeholder="Masukkan Kategori">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
</div>
</div>
@endsection
@extends('layouts.master')
@section('content')
<div class="container my-2">
  @auth
  <a href="/berita/create" class="btn btn-primary my-2">Tambah Berita</a>
  @endauth
  <table class="table">
      <thead class="thead-light">
          <tr>
              <th scope="col">No</th>
              <th scope="col">Judul Berita</th>
              <th scope="col">Kategori</th>
              <th scope="col">Penulis</th>
              <th scope="col">Aksi</th>
          </tr>
      </thead>
      <tbody>
          @forelse ($berita as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
                  <td>{{$value->judul}}</td>
                  <td>{{$value->kategori->nama}}</td>
                  <td>{{$value->user->name}}</td>
                  <td>
                    @if ($value->user->id === Auth::user()->id)
                    <form action="/berita/{{$value->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a>
                      <a href="/berita/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                      <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                      @else
                          <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a> 
                        @endif

                  </td>
                      </tr>
                      @empty
                      <tr colspan="3">
                          <td>No data</td>
                      </tr>  
                      @endforelse              
                  </tbody>
              </table>
          </div>
@endsection
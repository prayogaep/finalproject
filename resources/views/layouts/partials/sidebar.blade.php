<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
      </div>
      <div class="sidebar-brand-text mx-3">Kelompok 3</div>
    </a>
    @auth
    <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link" href="/berita">
          <i class="fas fa-fw fa-book"></i>
          <span>Berita</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/kategori">
          <i class="fas fa-fw fa-book"></i>
          <span>Kategori</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/profile">
          <i class="fas fa-fw fa-book"></i>
          <span>Profile</span></a>
      </li>
    @endauth
    <!-- Divider -->
    @guest
    <hr class="sidebar-divider">
    <li class="nav-item">
      <a class="nav-link" href="/berita">
        <i class="fas fa-fw fa-book"></i>
        <span>Berita</span></a>
    </li>
    @endguest
    

    <!-- Nav Item - Dashboard -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    @auth
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
            <i class="nav-icon fas fa-power-off" aria-hidden="true"></i>
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
        </li>
              
          @endauth
    @guest
    <li class="nav-item">
      <a class="nav-link" href="/login">
        <i class="fas fa-fw fa-arrow-right"></i>
        <span>Login</span></a>
    </li>
    @endguest
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>


  <!-- End of Sidebar -->
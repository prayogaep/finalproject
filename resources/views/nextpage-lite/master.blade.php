<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Final Project - Portal Berita</title>
    
    <!-- favicon -->
    <link rel=icon href="{{asset ('nextpage-lite/assets/img/favicon.png')}}" sizes="20x20">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('nextpage-lite/assets/css/vendor.css')}}">
    <link rel="stylesheet" href="{{ asset ('nextpage-lite/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset ('nextpage-lite/assets/css/responsive.css')}}">

</head>
<body>

    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>

    <!-- search popup start-->
    <div class="td-search-popup" id="td-search-popup">
        <form action="index.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- search popup end-->
    <div class="body-overlay" id="body-overlay"></div>

    <!-- header start -->
    <div class="navbar-area">
        <!-- topbar end-->
        <!-- topbar end-->

        <!-- adbar end-->
       
        <!-- adbar end-->

        <!-- navbar start -->
        <nav class="navbar navbar-expand-lg">
            <div class="container nav-container">
                <div class="responsive-mobile-menu">
                    <div class="logo d-lg-none d-block">
                        <a class="main-logo" href="index.html"><img src="{{asset('nextpage-lite/assets/img/logo.png')}}" alt="img"></a>
                    </div>
                    <button class="menu toggle-btn d-block d-lg-none" data-target="#nextpage_main_menu" 
                    aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-left"></span>
                        <span class="icon-right"></span>
                    </button>
                </div>
                <div class="nav-right-part nav-right-part-mobile">
                    <a class="search header-search" href="#"><i class="fa fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="nextpage_main_menu">
                    <ul class="navbar-nav menu-open">
                        <li class="current-menu-item">
                            <a href="#">Dashboard</a>
                        </li>                        
                        <li class="current-menu-item">
                            <a href="/berita">Berita</a>
                        </li>                        
                        <li class="current-menu-item">
                            <a href="/kategori">Kategori Berita</a>
                        </li>                        
                        <li class="current-menu-item">
                            <a href="#">My posts</a>
                        </li>                        
                        <li class="current-menu-item">
                            <a href="#"> Profile</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-right-part nav-right-part-desktop">
                    <div class="menu-search-inner">
                        <input type="text" placeholder="Search For">
                        <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- navbar end -->

    <!-- banner area start -->
    @yield('content')
    <!-- banner area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->

    <!-- all plugins here -->
    <script src="{{ asset ('nextpage-lite/assets/js/vendor.js')}}"></script>
    <!-- main js  -->
    <script src="{{ asset ('nextpage-lite/assets/js/main.js')}}"></script>
</body>
</html>